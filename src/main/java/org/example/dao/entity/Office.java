package org.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = "offices")
@NoArgsConstructor
@AllArgsConstructor
public class Office extends AbstractAuditingEntity {

    @Id
    @Column(name = "officeCode")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String officeCode;

    @Column(name = "city")
    private String city;

    @Column(name = "phone")
    private String phone;

    @Column(name = "addressLine1")
    private String addressLine1;

    @Column(name = "addressLine2")
    private String addressLine2;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "postalCode")
    private String postalCode;

    @Column(name = "territory")
    private String territory;

    @OneToMany(mappedBy = "office")
    private Set<Employee> employees;

}
