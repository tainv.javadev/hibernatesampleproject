package org.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = "products")
@NoArgsConstructor
@AllArgsConstructor
public class Product extends AbstractAuditingEntity {

    @Id
    @Column(name = "productCode")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String productCode;

    @Column(name = "productName")
    private String productName;

    @Column(name = "productScale")
    private String productScale;

    @Column(name = "productVendor")
    private String productVendor;

    @Column(name = "productDescription")
    private String productDescription;

    @Column(name = "quantityInStock")
    private Integer quantityInStock;

    @Column(name = "buyPrice")
    private Integer buyPrice;

    @Column(name = "MSRP")
    private Integer MSRP;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private Set<OrderDetail> orderDetails;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "productLine")
    private ProductLine productLine;
}
