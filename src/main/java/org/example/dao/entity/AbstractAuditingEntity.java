package org.example.dao.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@MappedSuperclass
//@Audited
@Getter
@Setter
//@EntityListeners(AuditEventListener.class)
public abstract class AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "deleted", nullable = false, columnDefinition = "bit default false")
    private boolean deleted = false;

    @Column(name = "createdBy", nullable = false,
            columnDefinition = "varchar(" + 255 + ") default '" + "SYSTEM" + "'",
            length = 255, updatable = false)
    private String createdBy;

    @Column(name = "lastModifiedBy", length = 255)
    private String lastModifiedBy;

    @Column(name = "createdDate", updatable = false,
            columnDefinition = "DATETIME(" + 255 + ") DEFAULT CURRENT_TIMESTAMP",
            length = 255)
    private Timestamp createdDate;

    @Column(name = "lastModifiedDate")
    private Timestamp lastModifiedDate;

}