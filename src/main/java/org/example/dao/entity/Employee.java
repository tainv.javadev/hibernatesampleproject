package org.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Table(name = "employees")
@NoArgsConstructor
@AllArgsConstructor
public class Employee extends AbstractAuditingEntity {

    @Id
    @Column(name = "employeeNumber")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String employeeNumber;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "extension")
    private String extension;

    @Column(name = "email")
    private String email;

    @Column(name = "jobTitle")
    private String jobTitle;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "reportsTo")
    private Employee employee;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    private Set<Employee> employees;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    private List<Customer> customers;

    @Column(name = "officeCode", insertable = false, updatable = false)
    private String officeCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "officeCode")
    private Office office;
}
