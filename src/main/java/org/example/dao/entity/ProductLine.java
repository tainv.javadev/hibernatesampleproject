package org.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = "productlines")
@NoArgsConstructor
@AllArgsConstructor
public class ProductLine extends AbstractAuditingEntity {

    @Id
    @Column(name = "productLine")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String productLine;

    @Column(name = "textDescription")
    private String textDescription;

    @Column(name = "htmlDescription")
    private String htmlDescription;

    @Column(name = "image")
    private String image;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productLine")
    private Set<Product> products;
}
