package org.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "payments")
@NoArgsConstructor
@AllArgsConstructor
public class Payment extends AbstractAuditingEntity {

    @Id
    @ManyToOne
    @JoinColumn(name = "customerNumber", referencedColumnName = "customerNumber")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Customer customer;

    @Id
    @Column(name = "checkNumber")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer checkNumber;

    @Column(name = "paymentDate")
    private Date paymentDate;

    @Column(name = "amount")
    private Integer amount;

}
