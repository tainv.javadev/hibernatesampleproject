package org.example.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "orderdetails")
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetail extends AbstractAuditingEntity {

    @Id
    @Column(name = "orderNumber")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderNumber;

    @Id
    @Column(name = "productCode")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String productCode;

    @Column(name = "quantityOrdered")
    private Integer quantityOrdered;

    @Column(name = "priceEach")
    private BigDecimal priceEach;

    @Column(name = "orderLineNumber")
    private Integer orderLineNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "orderNumber", insertable = false, updatable = false)
    private Order order;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "productCode", insertable = false, updatable = false)
    private Product product;
}
