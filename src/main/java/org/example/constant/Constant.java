package org.example.constant;

public class Constant {

    public static Integer MAX = Integer.MAX_VALUE;

    public static Integer MIN = Integer.MIN_VALUE;

    public static class Gender {
        public static String MALE = "M";
        public static String FEMALE = "F";
    }

}
