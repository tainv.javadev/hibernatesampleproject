package org.example.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public class Enum {

    @Getter
    @AllArgsConstructor
    public enum Gender {

        MALE(1, "M", "Male"),
        FE_MALE(2, "F", "Female");

        private Integer numCode;
        private String strCode;
        private String description;
        private static final Map<Integer, String> genderMap= new HashMap<>();

        static {
            for (Enum.Gender gender : values()) {
                genderMap.put(gender.numCode, gender.strCode);
            }
        }

        public static String getStrCode(Integer numCode) {
            if (genderMap.containsKey(numCode)) return genderMap.get(numCode);
            return null;
        }
    }
}
