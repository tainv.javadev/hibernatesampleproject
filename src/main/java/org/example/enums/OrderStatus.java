package org.example.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatus {

    ON_HOLD("On Hold", "On Hold"),
    IN_PROCESS("In Process", "In Process"),
    CANCELLED("Cancelled", "Cancelled"),
    RESOLVED("Resolved", "Resolved"),
    SHIPPED("Shipped", "Shipped");

    private String code;
    private String description;
}
