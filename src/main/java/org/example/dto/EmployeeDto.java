package org.example.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto {

    private String employeeNumber;

    private String lastName;

    private String firstName;

    private String jobTitle;

    private String addressLine1;

    private String addressLine2;

    @Override
    public String toString() {
        return "ID: " + this.employeeNumber + "\n" +
                "NAME: " + this.firstName + "." + this.lastName + "\n" +
                "JOB TITLE: " + this.jobTitle + "\n" +
                "ADDRESS_1: " + (this.addressLine1 == null ? "" : this.addressLine1) + "\n" +
                "ADDRESS_2: " + (this.addressLine2 == null ? "" : " - " + this.addressLine2);
    }

}
