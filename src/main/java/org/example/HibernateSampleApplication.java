package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.constant.Constant;
import org.example.dao.entity.Customer;
import org.example.dao.entity.Employee;
import org.example.dao.entity.Order;
import org.example.dao.entity.Product;
import org.example.dto.EmployeeDto;
import org.example.enums.Enum;
import org.example.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class HibernateSampleApplication {

    public static void main(String[] args) {

    }


    private static void printOder(List<Order> orders) {
        for (Order order : orders) {
            System.out.println("OderId: " + order.getOrderNumber() + "\nTen khach hang: " + order.getCustomer().getCustomerName() + "\nNgay order: "  + order.getOrderDate());
            System.out.println();
        }
    }

    public static void printListEmployee(List<EmployeeDto> employeeDtos) {
        for(EmployeeDto employeeDto : employeeDtos) {
            System.out.println(employeeDto.toString());
            System.out.println();
        }
    }

    public static List<EmployeeDto> getListEmployees() {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        List<Employee> employeeList = session.createQuery("Select e from Employee e ").list();
        session.close();
        return employeeList.stream().map(employee -> {
            return EmployeeDto.builder()
                    .employeeNumber(employee.getEmployeeNumber())
                    .firstName(employee.getFirstName())
                    .lastName(employee.getLastName())
                    .jobTitle(employee.getJobTitle())
                    .addressLine1(employee.getOffice().getAddressLine1())
                    .addressLine2(employee.getOffice().getAddressLine2())
                    .build();
        }).collect(Collectors.toList());
    }

    public static EmployeeDto getEmployeeById(String id) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Employee employee = (Employee) session.get(Employee.class, id);

        session.close();
        return EmployeeDto.builder()
                .employeeNumber(employee.getEmployeeNumber())
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .jobTitle(employee.getJobTitle())
                .addressLine1(employee.getOffice().getAddressLine1())
                .addressLine2(employee.getOffice().getAddressLine2())
                .build();
    }



    private static List<Order> getListOrderByCustomerNumber(Integer customerNumber) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
//        Customer customer = (Customer) session.createQuery("Select c From Customer c Where c.customerNumber = :customerNumber ")
//                .setInteger("customerNumber", customerNumber)
//                .uniqueResult();
        List<Order> orders = session.createQuery("Select order From Order order Where order.customer.customerNumber = :customerNumber and order.status <> 'Shipped' ")
                .setInteger("customerNumber", customerNumber).list();
//        List<Order> orders = session.createQuery("Select order From Order order Where order.customerNumber = :customerNumber")
//                .setInteger("customerNumber", customerNumber).list();
//        List<Order> orders = session.createQuery("Select c.orders From Customer c Where c.customerNumber = :customerNumber ")
//                .setInteger("customerNumber", customerNumber)
//                .list();

        session.close();
        return orders;
    }
}